<?php

/****************************************************************************
* This class file is used for PDO_mysql database connections and operations *
****************************************************************************/

class SimplePDO
{

    var $host      =  "localhost"; // Database Host
    var $username  =  'root';      // Database User
    var $password  =  '';          // Database Password
    var $dbname    =  "database";  // Database Name

/**************************************************
* The main constructor to initialize the function *
**************************************************/

function SimplePDO()
 {
 	try{
   $this->con = new PDO("mysql:host=".$this->host.";dbname=".$this->dbname."",$this->username,$this->password);
   return $this->con;
       }
 	   catch(PDOException $e) {return $e->getMessage();}
 }

/****************************************************
* Select the function with a PDO prepared execution *
****************************************************/

 function select($sql)
 {
 	try{
		$sth = $this->con->prepare($sql);
		$sth->execute();
		$result = $sth->fetchAll();
		return $result;
    	}
 	catch(PDOException $e) {
 		return $e->getMessage();
 		}
 }

/*****************************************************
* Function to INSERT values via PDO into MySQL Table *
*****************************************************/

function insert($table,$values)
{
	try{	
		$this->tableName =trim($table);
		$this->value = $values;
        if(!is_array($this->value)){return 0;}
	    $count = 0;
	    foreach($this->value as $key => $val )
	    {
	    if($count == 0)
			          { $this->field1 = ":".$key."";
			          $this->field2 = "`".$key."`";
				      $this->fieldsValues = $val;
				      }
				  else{
					$this->fieldsValues.= ", ".$val." ";
					$this->field1.= ",:".$key."";
					$this->field2.= ",`".$key."`";
				    }
				$count++;
		}
	$this->query =sprintf("insert into %s (%s) values (%s)",$this->tableName,$this->field2,$this->fieldsValues);
	$res=$this->con->query($this->query); 
	return $this->con->lastInsertId(); 
     }
	catch(PDOException $e){return $e->getMessage();}
}

/*****************************************************
* Function to UPDATE values via PDO in a MySQL Table *
*****************************************************/

function update($table , $values , $where = 1 , $limit = 1)
{
	try{	
		$this->tableName = trim($table);
		$this->value = $values;
		$this->where = $where;
		$this->limit = $limit;
		if(!is_array($this->value)){return 0;}
       $count = 0;
        $this->query = 'update '.$this->tableName.' set ';
        foreach($this->value as $key => $val )
            {
		    if($count == 0){$this->query.=" `$key`= ".$val." " ;}
			else{$this->query.=" , `$key`= ". $val ." " ;}
			$count++;
		    }
		$this->query.="  WHERE $this->where  LIMIT $this->limit ";
        $res=$this->con->query($this->query);	
	      return 1;
      }
	catch(PDOException $e)
     {return $e->getMessage();}
}

/*****************************************************
* Function to DELETE values via PDO from MySQL Table *
*****************************************************/

function delete($table,$where)
{
	try
	{
	    $this->table = trim($table);
		$this->where = $where;	
		$this->query = "DELETE FROM ".$this->table." WHERE ".$this->where;
        $res=$this->con->query($this->query);	
	    return $res; 
    }catch(PDOException $e)
     {return $e->getMessage();}

}

/******************************************************
* Function to close the PDO MySQL database connection *
******************************************************/

function close(){try{$this->con=null;}catch(PDOException $e){return $e->getMessage();}}

/*****************************************************************
* Function to insert safe HTML entities into MySQL table via PDO *
*****************************************************************/

function sqlSafe($value, $quote="'")
	{try{
		$value = str_replace(array("\'","'"),"&#39;",$value);
	     if (get_magic_quotes_gpc()){$value = stripslashes($value);}
	     $value = $quote . $value . $quote;
         return $value;
	   }catch(PDOException $e)
        {return $e->getMessage();}
  }
}

/*********************************************************
* End of Main Class - Now initialize object to be called *
**********************************************************/

$db = new SimplePDO();

?> 
