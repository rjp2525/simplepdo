<?php
/**************************************
* Table Declaration and Class Include *
**************************************/
include("simplepdo.class.php");

$table="example";

echo "SimplePDO Example Database Abstractions<br>";                                  // Add some text so we know what this page is doing

/********************************************************************
* Select the first instance before modification and display a table *
********************************************************************/

echo "Selecting the first instance before modification and displaying a table<br>";  // What this HTML table is displaying
 $sql = "SELECT * FROM ".$table."";                                                  // Query the MySQL table
 $row =$db->select($sql);                                                            // Select the MySQL results
 echo '<table width="80%" border="1">';                                              // Opening tag for the HTML table
	echo '<tr>';                                                                     // Open the column header row
		echo'<td>ID</td>';                                                           // Column header cell for the IDs
		echo'<td>Name</td>';                                                         // Column header cell for the names
		echo'<td>Email</td>';                                                        // Column header cell for the emails
		echo'<td>Birth Date</td>';                                                   // Column header cell for the birth dates
		echo'<td>Country</td>';                                                      // Column header cell for the countries
	echo '</tr>';                                                                    // Close the column header row
for($i=0;$i<count($row);$i++){                                                       // Get the number of rows to display
	echo "<tr>" ;                                                                    // Create a new row
		echo "<td>".$row[$i]['id']."</td>";                                          // Create a new cell for the IDs
		echo "<td>".$row[$i]['name']."</td>";                                        // Create a new cell for the names
		echo "<td>".$row[$i]['email']."</td>";                                       // Create a new cell for the emails
		echo "<td>".$row[$i]['date']."</td>";                                        // Create a new cell for the birth dates
		echo "<td>".$row[$i]['country']."</td>";                                     // Create a new cell for the countries
	echo "</tr>";                                                                    // Close the row
}
echo "</table>";                                                                     // Closing table tag

/*************************************
* Example of Data Insertion Function *
*************************************/

echo "Example of data insertion<br>";                                                // What this action is doing
$val= $db->sqlsafe('Arthur');                                                        // Filter the results, create safe data to insert
$ar = array ('name' =>$val);                                                         // Create an array for the column to insert to
$res = $db->insert($table,$ar);                                                      // Insert the new data
if($res!=0){
	echo 'Insertion of data was successful.<br>Most recent insertion:'.$res;         // If it was successful, show us what we inserted
	} else {
	echo "No data was inserted.<br>";                                                // If it failed, this will let us know
	}

/********************************************************
* Example of update function - First row in MySQL table *
********************************************************/

echo "Example of updating the name data (Uriel) in the first row<br>";               // Explaining the actions of this function
$val2= $db->sqlsafe('Jennifer');                                                     // Filter the results, create safe data to insert
$ar2 = array ('name' =>$val2);                                                       // Create an array for the column to insert to
$where='id = 1';                                                                     // Condition of which row to insert the updated data into
 $res = $db->update($table, $ar2 , $where);                                          // Perform the update action
if($res==1){
	echo 'Successfully updated the cell!<br>Updated "'.$res.'"';                     // If it was successful, show us what information we updated
	} else {
	echo "No data was updated.<br>";                                                 // If it failed, this will let us know.
	}

/**************************************************
* Example of row deletion - Row #5 in MySQL table *
**************************************************/

echo 'Example of row #5 deletion in the MySQL table<br>';                            // Tell us what's going on here
$where='typelead_id = 33';                                                           // Condition to select the row ID
$res= $db->delete($table,$where);                                                    // Perform the deletion action
echo $res;                                                                           // Display the deletion action
if($res==TRUE){
	echo 'Deleted row successfully!<br>';                                            // Text to show if we successfully deleted the row
	} else {
	echo "No information was deleted from the database.<br>";                        // If it failed to delete, tell us
	}

/********************************************************
* Select the data after insertion / updating & deleting *
********************************************************/

echo "Select data from the table once the above functions have been performed<br>";  // Tell us what's in this table
 $sql = "SELECT * FROM ".$table."";                                                  // Query the MySQL table
 $row =$db->select($sql);                                                            // Select the MySQL results
 echo '<table width="80%" border="1">';                                              // Opening tag for the HTML table
	echo '<tr>';                                                                     // Open the column header row
		echo'<td>ID</td>';                                                           // Column header cell for the IDs
		echo'<td>Name</td>';                                                         // Column header cell for the names
		echo'<td>Email</td>';                                                        // Column header cell for the emails
		echo'<td>Birth Date</td>';                                                   // Column header cell for the birth dates
		echo'<td>Country</td>';                                                      // Column header cell for the countries
	echo '</tr>';                                                                    // Close the column header row
for($i=0;$i<count($row);$i++){                                                       // Get the number of rows to display
	echo "<tr>" ;                                                                    // Create a new row
		echo "<td>".$row[$i]['id']."</td>";                                          // Create a new cell for the IDs
		echo "<td>".$row[$i]['name']."</td>";                                        // Create a new cell for the names
		echo "<td>".$row[$i]['email']."</td>";                                       // Create a new cell for the emails
		echo "<td>".$row[$i]['date']."</td>";                                        // Create a new cell for the birth dates
		echo "<td>".$row[$i]['country']."</td>";                                     // Create a new cell for the countries
	echo "</tr>";                                                                    // Close the row
}
echo "</table>";                                                                     // Closing table tag

/**************************************
* Close the MySQL database connection *
**************************************/

$db->close();                                                                        // Close the database connection

?>
