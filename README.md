#SimplePDO

This class simplifies MySQL database manipulation using PDO functions. Provides fast, simple and easy database abstractions.

## Example Usage / Installation

 1. Create a MySQL database
 
 2. Create a table within the database
 
 3. Modify the connection information in simplepdo.class.php
 
 4. Run this MySQL query to create the table columns:

   <pre><code>CREATE TABLE IF NOT EXISTS `example` (
     `id` mediumint,
     `name` varchar(255) default NULL,
     `email` varchar(255) default NULL,
     `date` varchar(255),
     `country` varchar(100) default NULL
   );</pre></code>

 5. Run the following query to insert some test values:

   <pre><code>INSERT INTO `example` (`id`,`name`,`email`,`date`,`country`) VALUES
   ("1","Uriel","felis.ullamcorper@Phasellus.net","Jul 16, 2008","Ecuador"),
   ("2","Cheryl","pharetra.ut.pharetra@mi.co.uk","Aug 14, 2011","Iraq"),
   ("3","Debra","eu@dis.ca","Sep 18, 2003","Malaysia"),
   ("4","Giacomo","et.malesuada.fames@ametcgcing.org","May 13, 2010","Nicaragua"),
   ("5","Mara","tempor.lorem@elementumpurus.com","Jul 5, 2013","Nepal"),
   ("6","Dai","volutpat.nunc.sit@magnanec.net","Oct 9, 2009","Korea"),
   ("7","Logan","pede.et.risus@etpedeNunc.co.uk","Jan 22, 2011","Argentina"),
   ("8","Olympia","Nam.ligula@In.ca","Dec 13, 2003","Austria");</pre></code>

 6. Place "simplepdo.php" and "simplepdo.class.php" together somewhere in your web root directory
 7. Navigate to "http://example.com/simplepdo.php" or location you placed the file
 8. If the webpage displays a table with the information inserted into the database, you have successfully installed this class! If not, the error message should explain where the problem is.
